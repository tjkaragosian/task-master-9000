package com.over9000.timkaragosian.taskmaster9001

import java.io.Serializable

data class Task(var description: String,
                var categorySelection: String,
                var date: String) : Serializable
