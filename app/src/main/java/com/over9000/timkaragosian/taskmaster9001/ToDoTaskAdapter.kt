package com.over9000.timkaragosian.taskmaster9001

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView

class ToDoTaskAdapter(private val mainActivity: MainActivity, tasks: MutableList<Task> = ArrayList()) : RecyclerView.Adapter<ToDoTaskAdapter.TaskViewHolder>() {

    var tasks: MutableList<Task> = tasks
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context)?.inflate(R.layout.list_item_todo_task, parent, false)

        return TaskViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bindTask(tasks[position])

        holder.deleteImageButton?.setOnClickListener {
            mainActivity.removeToDoItem(position)
            notifyDataSetChanged()
        }
        holder.completedImageButton?.setOnClickListener {
            mainActivity.completedToDoItem(position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    inner class TaskViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        val descriptionTextView = view?.findViewById<TextView>(R.id.task_description)
        val categoryTextView = view?.findViewById<TextView>(R.id.task_category_textview)
        val completedImageButton = view?.findViewById<ImageButton>(R.id.task_completed)
        val deleteImageButton = view?.findViewById<ImageButton>(R.id.task_delete)

        fun bindTask(task: Task) {
            descriptionTextView?.text = task.description
            categoryTextView?.text = task.categorySelection
        }
    }

    fun addTask(task: Task) {
        tasks.add(0, task)
        notifyDataSetChanged()
    }
}