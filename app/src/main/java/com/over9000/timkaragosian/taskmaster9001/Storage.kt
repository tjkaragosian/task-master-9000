package com.over9000.timkaragosian.taskmaster9001

import android.content.Context
import android.util.Log
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

object Storage {
    private val LOG_TAG = Storage::class.java.simpleName
    private val FILE_NAME_TODO = "todo_list.ser"
    private val FILE_NAME_COMPLETED = "completed_list.ser"

    fun writeDataToDo(context: Context, tasks: List<Task>?) {
        var fos: FileOutputStream? = null
        var oos: ObjectOutputStream? = null

        try {
            //Open file and write list to it
            fos = context.openFileOutput(FILE_NAME_TODO, Context.MODE_PRIVATE)
            oos = ObjectOutputStream(fos)
            oos.writeObject(tasks)
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Error: Object not written to file")
            e.printStackTrace()
        } finally {
            try {
                oos?.close()
                fos?.close()
            } catch (e: Exception) {
                Log.e(LOG_TAG, "Error: Cannot close file")
                e.printStackTrace()
            }
        }
    }

    fun readDataToDo(context: Context): MutableList<Task>? {
        var fis: FileInputStream? = null
        var ois: ObjectInputStream? = null

        var tasks: MutableList<Task>? = ArrayList()

        try {
            //Open file and read data
            fis = context.openFileInput(FILE_NAME_TODO)
            ois = ObjectInputStream(fis)

            tasks = ois?.readObject() as? MutableList<Task>
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Error: Could not read data from file")
            e.printStackTrace()
        } finally {
            try {
                ois?.close()
                fis?.close()
            } catch (e: Exception) {
                Log.e(LOG_TAG, "Error: Could not close file")
                e.printStackTrace()
            }
        }
        return tasks
    }

    fun writeDataCompleted(context: Context, tasks: List<Task>?) {
        var fos: FileOutputStream? = null
        var oos: ObjectOutputStream? = null

        try {
            //Open file and write list to it
            fos = context.openFileOutput(FILE_NAME_COMPLETED, Context.MODE_PRIVATE)
            oos = ObjectOutputStream(fos)
            oos.writeObject(tasks)
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Error: Object not written to file")
            e.printStackTrace()
        } finally {
            try {
                oos?.close()
                fos?.close()
            } catch (e: Exception) {
                Log.e(LOG_TAG, "Error: Cannot close file")
                e.printStackTrace()
            }
        }
    }

    fun readDataCompleted(context: Context): MutableList<Task>? {
        var fis: FileInputStream? = null
        var ois: ObjectInputStream? = null

        var tasks: MutableList<Task>? = ArrayList()

        try {
            //Open file and read data
            fis = context.openFileInput(FILE_NAME_COMPLETED)
            ois = ObjectInputStream(fis)

            tasks = ois?.readObject() as? MutableList<Task>
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Error: Could not read data from file")
            e.printStackTrace()
        } finally {
            try {
                ois?.close()
                fis?.close()
            } catch (e: Exception) {
                Log.e(LOG_TAG, "Error: Could not close file")
                e.printStackTrace()
            }
        }
        return tasks
    }
}