package com.over9000.timkaragosian.taskmaster9001

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView


class AddTaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)

        val description = findViewById<EditText>(R.id.task_description)
        val category = findViewById<Spinner>(R.id.task_category_selection)
        val submit = findViewById<Button>(R.id.submit)

        submit?.setOnClickListener {
            if (description?.text?.toString().isNullOrBlank() || category?.selectedItemPosition == 0) {
                if (description?.text?.toString().isNullOrBlank()) {
                    description?.error = "Please Enter a description"
                }

                if (category?.selectedItemPosition == 0) {
                    val errorText = category.getSelectedView() as TextView
                    errorText.error = getString(R.string.category_selection_error)
                    errorText.setTextColor(Color.RED)
                    errorText.text = getString(R.string.category_selection_error)
                }
            } else {
                val data = Intent()
                data.putExtra(MainActivity.DESCRIPTION_TEXT, description?.text?.toString())
                data.putExtra(MainActivity.CATEGORY_SELECTION, category?.selectedItem.toString())

                setResult(Activity.RESULT_OK, data)
                finish()
            }
        }
    }
}