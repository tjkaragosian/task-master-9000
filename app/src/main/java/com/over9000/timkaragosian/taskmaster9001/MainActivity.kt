package com.over9000.timkaragosian.taskmaster9001

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.time.LocalDate

class MainActivity : AppCompatActivity() {

    val toDoAdapter = ToDoTaskAdapter(this)
    val completedAdapter = CompletedTaskAdapter()
    var currentView = 0 //0 = To Do, 1 = Completed

    var isFabMenuShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toDoLayoutManager = LinearLayoutManager(this)
        val completedLayoutManager = LinearLayoutManager(this)

        val toDoRecyclerView = findViewById<RecyclerView>(R.id.todo_task_list)
        val completedRecyclerView = findViewById<RecyclerView>(R.id.completed_task_list)

        toDoRecyclerView.layoutManager = toDoLayoutManager
        toDoRecyclerView.adapter = toDoAdapter

        completedRecyclerView.layoutManager = completedLayoutManager
        completedRecyclerView.adapter = completedAdapter

        fab_menu.setOnClickListener { view ->
            if (!isFabMenuShown) {
                showFabMenu()
            } else {
                hideFabMenu()
            }
        }

        add_task_fab.setOnClickListener { view ->
            val intent = Intent(this, AddTaskActivity::class.java)
            startActivityForResult(intent, ADD_TASK_REQUEST)
        }

        switch_view_fab.setOnClickListener { view ->
            if (currentView == 0) {
                setViewCompleted()
                currentView = 1
            } else {
                setViewToDo()
                currentView = 0
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt("current_view", currentView)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        todo_task_list.adapter = toDoAdapter
        completed_task_list.adapter = completedAdapter

        if (savedInstanceState?.getInt("current_view", 0) == 0) {
            setViewToDo()
        } else {
            setViewCompleted()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ADD_TASK_REQUEST && resultCode == Activity.RESULT_OK) {
            val task = Task(data?.getStringExtra(DESCRIPTION_TEXT).orEmpty(), data?.getStringExtra(CATEGORY_SELECTION).orEmpty(), "") //date not used in adding tasks
            toDoAdapter.addTask(task)
            toDoAdapter.notifyDataSetChanged()
            Storage.writeDataToDo(this, toDoAdapter.tasks)
        }
    }

    companion object {
        private val ADD_TASK_REQUEST = 0
        val DESCRIPTION_TEXT = "description"
        val CATEGORY_SELECTION = "categorySelection"
    }

    override fun onResume() {
        super.onResume()
        val toDoTasks = Storage.readDataToDo(this)
        val completedTasks = Storage.readDataCompleted(this)

        if (toDoTasks != null && toDoAdapter.tasks.isEmpty()) {
            toDoAdapter.tasks = toDoTasks
        }

        if (completedTasks != null && completedAdapter.tasks.isEmpty()) {
            completedAdapter.tasks = completedTasks
        }

        /*if (toDoAdapter?.tasks.size > 0 && !TextUtils.isEmpty(toDoAdapter.tasks[toDoAdapter.tasks.size - 1].description)) {
            var task = Task("", "", "")
            toDoAdapter?.tasks.add(task)
            toDoAdapter?.tasks.add(task)
        }*/

        if (completedAdapter.tasks.size > 0 && !TextUtils.isEmpty(completedAdapter.tasks[completedAdapter.tasks.size - 1].description)) {
            var task = Task("", "", "")
            completedAdapter.tasks.add(task)
            completedAdapter.tasks.add(task)
        }
    }

    fun removeToDoItem(position: Int) {
        toDoAdapter.tasks.removeAt(position)
        Storage.writeDataToDo(this, toDoAdapter.tasks)
    }

    fun completedToDoItem(position: Int) {
        val completedTask = toDoAdapter.tasks[position]
        removeToDoItem(position)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            completedTask.date = LocalDate.now().dayOfMonth.toString() + "/" + LocalDate.now().monthValue.toString() + "/" + LocalDate.now().year.toString()
        }

        completedAdapter.addCompletedTask(completedTask)
        Storage.writeDataCompleted(this, completedAdapter.tasks)
    }

    fun setViewCompleted() {
        todo_relative_layout.visibility = View.GONE
        completed_relative_layout.visibility = View.VISIBLE
        todo_task_list.visibility = View.GONE
        completed_task_list.visibility = View.VISIBLE
        fab_menu.visibility = View.GONE

        add_task_linear_layout.visibility = View.GONE

        switch_view_linear_layout.visibility = View.VISIBLE
        switch_view_linear_layout.animate().translationY(0f)

        title_textview.text = "Completed Tasks"
    }

    fun setViewToDo() {
        todo_relative_layout.visibility = View.VISIBLE
        completed_relative_layout.visibility = View.GONE
        todo_task_list.visibility = View.VISIBLE
        completed_task_list.visibility = View.GONE
        fab_menu.visibility = View.VISIBLE
        switch_view_button_textview.visibility = View.INVISIBLE

        title_textview.text = "To Do Tasks"
        isFabMenuShown = false
    }

    fun showFabMenu() {
        add_task_linear_layout.visibility = View.VISIBLE
        switch_view_linear_layout.visibility = View.VISIBLE

        switch_view_button_textview.visibility = View.VISIBLE
        add_task_button_textview.visibility = View.VISIBLE

        add_task_linear_layout.animate().translationY(-140f)
        switch_view_linear_layout.animate().translationY(-260f)

        isFabMenuShown = true
    }

    fun hideFabMenu() {
        add_task_linear_layout.animate().translationY(0f)
        switch_view_linear_layout.animate().translationY(0f)

        switch_view_button_textview.visibility = View.INVISIBLE
        add_task_button_textview.visibility = View.INVISIBLE

        isFabMenuShown = false
    }

    /*TODO:
        - BUILD SIMPLE TO DO APP IN KOTLIN USING THIS AS A TUTORIAL https://androidessence.com/android/how-to-build-a-todo-list-in-kotlin-part-1-new-project/
        - BUILD OFF OF THAT FOR MY NEEDS BY ADDING A SPINNER FOR THE TYPE OF TASK AND WHEN COMPLETED AND IS THE NEXT DAY, REMOVE FROM LIST
        - PUT IN NEW FIELDS FOR TO DO ITEM
        - HAVE INTERFACE WITH VEHICLES THAT USE ANDROID AUTO
        - BUILD SIGN IN FUNCTIONALITY AND DOWNLOAD SAVED TASKS AND HAVE IT STORE ON SERVER AS WELL AS LOCAL IN THE DB
        - INCORPORATE PUSH NOTIFICATIONS
        - CONVERT TO MVVM
        - UTILIZE RXANDROID (https://medium.freecodecamp.org/rxandroid-and-kotlin-part-1-f0382dc26ed8)
        - UTILIZE RETROFIT 2 (https://medium.com/@elye.project/kotlin-and-retrofit-2-tutorial-with-working-codes-333a4422a890)
        - UTILIZE DAGGER 2 (https://medium.com/@elye.project/dagger-2-for-dummies-in-kotlin-with-one-page-simple-code-project-618a5f9f2fe8)
        - TIME TO COMPLETE: 2 MONTHS OR LESS
    */
}
