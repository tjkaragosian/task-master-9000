package com.over9000.timkaragosian.taskmaster9001

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class CompletedTaskAdapter(tasks: MutableList<Task> = ArrayList()) : RecyclerView.Adapter<CompletedTaskAdapter.TaskViewHolder>() {

    var tasks: MutableList<Task> = tasks
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context)?.inflate(R.layout.list_item_completed_task, parent, false)

        return TaskViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bindTask(tasks[position])
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    inner class TaskViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        val descriptionCompletedTextView = view?.findViewById<TextView>(R.id.task_completed_description)
        val categoryCompletedTextView = view?.findViewById<TextView>(R.id.category_completed_textview)
        val dateCompletedTextView = view?.findViewById<TextView>(R.id.task_completed_date)

        fun bindTask(task: Task) {
            descriptionCompletedTextView?.text = task.description
            categoryCompletedTextView?.text = task.categorySelection
            dateCompletedTextView?.text = task.date
        }
    }

    fun addCompletedTask(task: Task) {
        tasks.add(0, task)
        notifyDataSetChanged()
    }
}
